-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 23, 2020 lúc 11:55 AM
-- Phiên bản máy phục vụ: 10.4.14-MariaDB
-- Phiên bản PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `weather`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `local`
--

CREATE TABLE `local` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `status` int(1) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `local`
--

INSERT INTO `local` (`id`, `name`, `status`) VALUES
(1, '24h', 1),
(2, 'Việt Nam Express', 1),
(3, 'Zing', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `news_link`
--

CREATE TABLE `news_link` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `local` int(11) NOT NULL,
  `url` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1,
  `isCheck` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `news_link`
--

INSERT INTO `news_link` (`id`, `name`, `local`, `url`, `status`, `isCheck`) VALUES
(1, 'Ẩm thực', 1, 'https://cdn.24h.com.vn/upload/rss/amthuc.rss', 1, 1),
(2, 'Giáo dục', 2, 'https://vnexpress.net/rss/giao-duc.rss', 1, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `old_news`
--

CREATE TABLE `old_news` (
  `id` int(11) NOT NULL,
  `url` text NOT NULL,
  `id_user` varchar(255) NOT NULL,
  `idNews` int(11) NOT NULL,
  `title` text NOT NULL,
  `isoDate` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `old_news`
--

INSERT INTO `old_news` (`id`, `url`, `id_user`, `idNews`, `title`, `isoDate`) VALUES
(16, 'https://vnexpress.net/chung-khoan-vuot-vung-gia-cuoi-nam-2019-4181270.html', 'admin', 0, 'Chứng khoán vượt vùng giá cuối năm 2019', '2020-10-23T08:49:46.000Z'),
(17, 'https://vnexpress.net/chung-khoan-vuot-vung-gia-cuoi-nam-2019-4181270.html', 'admin', 0, 'Chứng khoán vượt vùng giá cuối năm 2019', '2020-10-23T08:49:46.000Z');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `iduser` varchar(255) NOT NULL,
  `password` text NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` int(1) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `iduser`, `password`, `create_at`, `status`) VALUES
(1, 'admin', 'admin', '123123', '2020-10-19 05:33:15', 1),
(2, 'admin2', 'admin2', '123123', '2020-10-19 05:33:15', 1),
(3, 'admin3', 'admin3', '1231233', '2020-10-19 05:33:15', 1),
(4, 'admin4', 'admin1', 'admon1', '2020-10-19 07:20:11', 1),
(5, 'das', 'dsa', 'dsa', '2020-10-19 07:30:35', 1),
(6, 'ad1', '1ad1', 'ad11', '2020-10-19 07:37:02', 1),
(7, 'ad1', '1ad1', 'ad1122', '2020-10-19 07:38:21', 1),
(8, 'ad1', '1ad1', 'ad11221', '2020-10-19 07:39:05', 1),
(9, 'ad11', '1ad1', 'ad11221', '2020-10-19 07:39:11', 1),
(10, 'admin10', 'admin10', '123123', '2020-10-23 05:39:37', 1);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `local`
--
ALTER TABLE `local`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `news_link`
--
ALTER TABLE `news_link`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `old_news`
--
ALTER TABLE `old_news`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `local`
--
ALTER TABLE `local`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `news_link`
--
ALTER TABLE `news_link`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `old_news`
--
ALTER TABLE `old_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
