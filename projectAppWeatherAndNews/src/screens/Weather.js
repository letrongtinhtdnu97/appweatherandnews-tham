import React from 'react'
import {Text, View,SafeAreaView,FlatList} from 'react-native'



const hn = require('../../assets/hn.jpg')
const hcm = require('../../assets/hcm.jpg')
const dn = require('../../assets/dn.jpg')
import { AntDesign } from '@expo/vector-icons'; 
import CardView from '../components/CardView'
const DATA = [
    {
      id: '1',
      title: 'Thông tin thời tiết ở Hà Nội',
      address: '1581129',
      name: "Hà Nội"
    },
    {
      id: '2',
      title: 'Thông tin thời tiết ở Hồ Chí Mình',
      address: '1566083',
      name: "Hồ Chí Minh"
    },
    {
      id: '3',
      title: 'Thông tin thời tiết ở Đà Nẵng',
      address: '1905468',
      name: "Đà Nẵng"
    }
    
  ];

const Weather = (props) => {
    
    if(!DATA) {
        return null
    }
   return(
    <SafeAreaView style={{flex:1, marginTop:32}}>
        <View style={{
            height:50,
            backgroundColor:'rgb(98, 0, 238)',
            flexDirection:'row',
            alignItems:"center",
            justifyContent:'center'
        }}>
            <Text style={{color:'white', fontSize:20}}>TRANG THỜI TIẾT</Text>
        </View>
        <FlatList
            data={DATA}
            renderItem={({item}) => 
                <View style={{padding:10}}>
                   <CardView navigation={props.navigation}  data={item}/>
                </View>
            }
            keyExtractor={item => item.id}
        />
    </SafeAreaView>

   );
}

export default Weather;
