import React, {useEffect, useState} from 'react'
import { StyleSheet, Text, View, SafeAreaView, Alert  } from 'react-native'
import { Avatar, Accessory,Input,Button   } 
    from 'react-native-elements';
import Screens from '../constants/screens'
import axios from 'axios'
import config from '../api/config'
const CallAPI = async (name,username, password) => {
    try {
        const body = {
            name,
            iduser: username,
            password
        }
        const result = await axios({
            method: 'post',
            url: `${config.url}/users/create`,
            data: body
        })

        if(result.data.code === 0) {
            return true
        }
        return false
    } catch (error) {
        return false
    }
}
const Register = (props) => {
    const [username, setUserName] = useState("")
    const [name, setName] = useState("")
    const [password, setPassWord] =useState("")
    const [cpassword, setCPassWord] =useState("")

    const handleSubmit = async()=> {
        if(password !== cpassword) {
            Alert.alert("Comfirm password error !")
            return 
        }
        const result =  await CallAPI(name,username,password)
        if(result) {
            return props.navigation.navigate(Screens.LOGIN)
        }else{
            Alert.alert("Khong the tao tai khoan nay!")
            return
        }
       
    }
    
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.header}>
                <Avatar 
                    size="xlarge" 
                    rounded 
                    icon={{ name: 'home' }} 
                />
                <Text style={styles.text}>Hello Welcome App</Text>
            </View>
            <View style={{
                width:400, 
                flexDirection:'column',
                justifyContent:'center',
                paddingTop:150
            }}>
                <Input
                    style={styles.input}
                    placeholder="Username"
                    leftIcon={{ type: 'font-awesome', name: 'user' }}
                    onChangeText={(text) => setName(text)}
                    />
                <Input
                    style={styles.input}
                    placeholder="Username"
                    leftIcon={{ type: 'font-awesome', name: 'user' }}
                    onChangeText={(text) => setUserName(text)}
                    />
                <Input
                    style={styles.input}
                    placeholder="Password"
                    secureTextEntry
                    leftIcon={{ type: 'font-awesome', name: 'comment' }}
                    onChangeText={(text) => setPassWord(text)}
                    />
                <Input
                    style={styles.input}
                    placeholder="Comfirm Password"
                    secureTextEntry
                    leftIcon={{ type: 'font-awesome', name: 'comment' }}
                    onChangeText={(text) => setCPassWord(text)}
                    />
                <Button
                    title="SIGN UP"
                    buttonStyle={{
                        borderWidth:1,
                        borderColor:'black',
                        height:50
                    }}
                    onPress={handleSubmit}
                    />
               
            </View>
            
        </SafeAreaView>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1, 
        backgroundColor:'#3578e5',
        alignItems:'center'
    },
    header: {
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'center',
        marginTop:150
    },
    text: {
        fontSize:20,
        color:'white'
    },
    input: {
        color:'white'
    }
})

export default Register;
