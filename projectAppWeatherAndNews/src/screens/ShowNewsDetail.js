import React , {useState, useEffect} from 'react';
import {AsyncStorage} from 'react-native';
import { WebView } from 'react-native-webview';
import axios from 'axios'
import config from '../api/config'
// ...
const CallAPI = async (idNews,link,title,date) => {
        
        try {
            
            const value = await AsyncStorage.getItem('name')
            
            const body = {
                id: value,
                link,
                id_news: idNews,
                date,
                title
            }
            console.log(body)
            const result = await axios({
                method: 'post',
                url: `${config.url}/rss/old-news`,
                data: body
            })
            
            return true
        } catch (error) {
            return false
        }
    }
const ShowView = (props) => {
    const [link, setLink] = useState('')
    const [idNews, setId] =useState('')
    const [title, setTitle] =useState('')
    const [date, setDate] =useState('')
    useEffect(() => {
        
        setLink(props?.route?.params?.url  ? props?.route?.params?.url : '')
        setId(props?.route?.params?.id_news ? props?.route?.params?.id_news : 0)
        setDate(props?.route?.params?.date ? props?.route?.params?.date : '')
        setTitle(props?.route?.params?.title ? props?.route?.params?.title : '')
    },[props])
    
    useEffect(() => {
        
        if(link) {
            CallAPI(idNews,link, title, date)
        }
    },[link,id])
    
    
    return <WebView source={{ uri: link }} />;
}

export default ShowView
