import React from 'react'
import { StyleSheet, Text, View,SafeAreaView } from 'react-native'

const Home = (props) => {
    return (
        <SafeAreaView style={{flex: 1, paddingTop:35}}>
            <Text>Home</Text>
        </SafeAreaView>
    )
}
const styles = StyleSheet.create({})

export default Home
