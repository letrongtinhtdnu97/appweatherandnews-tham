import React,{useState, useEffect} from 'react'
import { StyleSheet, Text, View,SafeAreaView,FlatList,AsyncStorage } from 'react-native'
import { FAB, Portal, Provider,Button } from 'react-native-paper';
import axios from 'axios'
import config from '../api/config'
import CardNews from '../components/CardNews'
import ViewWeather from '../components/ViewWeather';
import Screens from '../constants/screens'
const Home = (props) => {
    const [state, setState] = React.useState({ open: false });
    const onStateChange = ({ open }) => setState({ open });
    const { open } = state;
    const [isNews, setNews] = useState(0)
    const [arrData , setArrData] = useState([])
    const [link,setLink] = useState(`${config}/rss/all-vne`)
    const [title, setTitle] =useState('')
    
    
    useEffect(() => {
        switch (isNews) {
            case 0:
                setTitle(Screens.title.VNE)
                setLink(`${config.url}/rss/all-vne`)
                break;
            case 1:
                setTitle(Screens.title.TT)
                setLink(`${config.url}/rss/all-tuoitre`)
                break;
            case 2:
                setTitle(Screens.title.H24)
                setLink(`${config.url}/rss/all-h24`)
                break;
            default:
                setTitle(Screens.title.VNE)
                setLink(`${config.url}/rss/all-vne`)
                break;
        }
    },[isNews])
    useEffect(() => {
        CallApi()
    }, [link])
    
    const CallApi = async() => {

        try {
            const result = await axios({
                method: 'get',
                url: link  
            })

            if(result?.data?.code === 0) {
                setArrData(result?.data?.data ? result?.data?.data : [] )
            }
        } catch (error) {
            setArrData([])
        }
    }
    
    
    const CallOLD = async() => {
       console.log(1)
         try {
            const value = await AsyncStorage.getItem('name')
            const body = {
                name: value,
                isNews
            }
            const result = await axios({
                method: 'post',
                url:  `${config.url}/rss/get-old-news` ,
                data: body
            })
            console.log(result)
            if(result?.data?.code === 0) {
                setArrData(result?.data?.data ? result?.data?.data : [] )
            }
        } catch (error) {
            setArrData([])
        }
    }
    
    return (
        <SafeAreaView style={{flex:1, marginTop:32}}>
            <View style={{
                height:50,
                backgroundColor:'rgb(98, 0, 238)',
                flexDirection:'row',
                alignItems:"center",
                justifyContent:'center'
            }}>
            <Text style={{color:'white', fontSize:20}}>{title ? title : ''}</Text>
            
            </View>
            <View style={{flexDirection:'row', justifyContent:'space-around', alignItems:'center', height:50}}>
                <Button icon="camera" mode="contained" onPress={()=>CallApi()}>
                    Mới nhất
                </Button>
                <Button icon="camera" mode="contained" onPress={()=>CallOLD()}>
                    Đã xem
                </Button>
  
            </View>
            <View>
                {
                    arrData.length === 0 ? 
                    <View style={{flex: 1, flexDirection:'row', justifyContent:'center', alignItems:'center', paddingTop: 20}}>
                        <Text>Không có thông tin nào cả</Text>
                    </View>
                    :
                    <FlatList 
                     data={arrData}
                     renderItem={({item}) => 
                         <View style={{padding:10}}>
                            <CardNews navigation={props.navigation} data={item} id_news={isNews} />
                         </View>
                     }
                     keyExtractor={({item,index}) => index}
                />
                }
            </View>
            <Provider>
                <Portal>
                    <FAB.Group
                        open={open}
                        icon={open ? 'calendar-today' : 'plus'}
                        actions={[
                            
                            {
                            icon: 'star',
                            label: Screens.title.VNE,
                            style: {
                                height:70,
                                width:70,
                                borderRadius:100,
                                flexDirection:'row',
                                alignItems:'center',
                                justifyContent:'center' 
                            }, 
                            onPress: () => setNews(0),
                            },
                            {
                            icon: 'email',
                            label: Screens.title.TT,
                            style: {
                                height:70,
                                width:70,
                                borderRadius:100,
                                flexDirection:'row',
                                alignItems:'center',
                                justifyContent:'center' 
                            }, 
                            onPress: () => setNews(1),
                            },
                            {
                                style: {
                                    height:70,
                                    width:70,
                                    borderRadius:100,
                                    flexDirection:'row',
                                    alignItems:'center',
                                    justifyContent:'center' 
                                }, 
                            icon: 'bell',
                            label: Screens.title.H24,
                            onPress: () => setNews(2),
                            },
                        ]}
                        fabStyle={{
                            height:100,
                            width:100,
                            borderRadius:100,
                            flexDirection:'row',
                            alignItems:'center',
                            justifyContent:'center'
                        }}
                        
                        onStateChange={onStateChange}
                        onPress={() => {
                            if (open) {
                            // do something if the speed dial is open
                            }
                        }}
                    />
                </Portal>
            </Provider>
        </SafeAreaView>
    )
}
const styles = StyleSheet.create({})

export default Home
