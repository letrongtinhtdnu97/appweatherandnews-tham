import React, {useEffect, useState} from 'react'
import { StyleSheet, Text, View, SafeAreaView, Alert ,AsyncStorage } from 'react-native'
import { Avatar, Accessory,Input,Button   } 
    from 'react-native-elements';
    
import Screens from '../constants/screens'
import axios from 'axios'
import config from '../api/config'
const CallAPI = async (username, password) => {
    try {
        const body = {
            id: username,
            password
        }
        const result = await axios({
            method: 'post',
            url: `${config.url}/users/get-user`,
            data: body
        })

        if(result.data.code === 0) {
            return true
        }
        return false
    } catch (error) {
        return false
    }
}
const Login = (props) => {
    const [username, setUserName] = useState("")
    const [password, setPassWord] =useState("")

    const handleSubmit = async()=> {
        try {
            const result =  await CallAPI(username,password)
            
                if(result) {
                    await AsyncStorage.setItem('name',username)
                    return props.navigation.navigate(Screens.TAB)
                }else{
                    Alert.alert("Username or password error!")
             }
        } catch (error) {
            Alert.alert("Username or password error!")
        }
      
       
    }
    
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.header}>
                <Avatar 
                    size="xlarge" 
                    rounded 
                    icon={{ name: 'home' }} 
                />
                <Text style={styles.text}>Hello Welcome App</Text>
            </View>
            <View style={{
                width:400, 
                flexDirection:'column',
                justifyContent:'center',
                paddingTop:150
            }}>
                <Input
                    style={styles.input}
                    placeholder="Username"
                    leftIcon={{ type: 'font-awesome', name: 'user' }}
                    onChangeText={(text) => setUserName(text)}
                    />
                <Input
                    style={styles.input}
                    placeholder="Password"
                    secureTextEntry
                    leftIcon={{ type: 'font-awesome', name: 'comment' }}
                    onChangeText={(text) => setPassWord(text)}
                    />
                <Button
                    title="LOGIN"
                    buttonStyle={{
                        borderWidth:1,
                        borderColor:'black',
                        height:50
                    }}
                    onPress={handleSubmit}
                    />
                <Button
                    title="REGISTER"
                    buttonStyle={{
                        borderWidth:1,
                        borderColor:'black',
                        height:50
                        
                    }}
                    containerStyle={{
                        paddingTop:20,
                        
                    }}
                    onPress={() => props.navigation.navigate(Screens.REG)}
                    />
            </View>
            
        </SafeAreaView>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1, 
        backgroundColor:'#3578e5',
        alignItems:'center'
    },
    header: {
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'center',
        marginTop:150
    },
    text: {
        fontSize:20,
        color:'white'
    },
    input: {
        color:'white'
    }
})

export default Login;
