import React , {useState,useEffect}from 'react'
import {View, Text} from 'react-native'
import { AntDesign } from '@expo/vector-icons'; 
import { Avatar, Button, Card, Title, Paragraph } from 'react-native-paper';
import Screens from '../constants/screens'
const LeftContent = props => <Avatar.Icon {...props} icon={
  ()=>  <AntDesign name="adduser" size={24} color="black" />
} />

const CardNews = (props) => {
    const [title,setTitle] = useState('')
    const [isoDate,setISODate] = useState('')
    const [link,setLink] = useState('')
    const [id_news, setid_news] = useState(0)
    useEffect(() => {
        setISODate(props?.data?.isoDate ? props?.data?.isoDate : '')
        setTitle(props?.data?.title ? props?.data?.title : '')
        setLink(props?.data?.link ? props?.data?.link : '')
        setid_news(props?.id_news ? props?.id_news : 0)
    },[props])
    
    const handleSubmit = () => {
        
        return props.navigation.navigate(Screens.WEB,{url: link, id_news: id_news, date: isoDate,title:title })
    } 
    return(
    <Card onPress={handleSubmit}>
        <Card.Title 
            title={title} 
            subtitle={`Ngay: ${isoDate}`} 
            left={LeftContent} 
        />
        
        
        {/* <Card.Actions>
            <Button>Cancel</Button>
            <Button>Ok</Button>
        </Card.Actions> */}
    </Card>
    );
}

export default CardNews;
