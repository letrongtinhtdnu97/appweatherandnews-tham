const { NativeModules } = require("react-native")

const LOGIN = "LOGIN"
const HOME = "HOME"
const NEWS = "NEWS"
const REG = "REG"
const WEATHER = "WEATHER"
const VWEATHER = "VWEATHER"
const WEB = "WEB"
const TAB = "TAB"
const url = (id) => {
    return  "http://api.openweathermap.org/data/2.5/weather?id="+id+"&appid=5fb61fd19922248eb6e530c47c2fd7ed"
}
const iconWeather = (icon) => {
    return ' http://openweathermap.org/img/wn/'+icon+'.png'
}
const title = {
    VNE: "Trang Báo VNExpress",
    TT: "Trang Báo Tuổi Trẻ",
    H24: "Trang Báo 24H.com"
}
module.exports = {
    LOGIN,
    HOME,
    NEWS,
    REG,
    WEATHER,
    url,
    iconWeather,
    VWEATHER,
    title,
    WEB,
    TAB
}