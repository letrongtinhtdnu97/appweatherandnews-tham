import * as React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {createBottomTabNavigator } from '@react-navigation/bottom-tabs'

import Home from './src/screens/Home';
import Login from './src/screens/Login';
import News from './src/screens/News';
import Register from './src/screens/Register';
import Weather from './src/screens/Weather';
import ViewWeather from './src/components/ViewWeather';
import ShowView from './src/screens/ShowNewsDetail'
import Screens from './src/constants/screens'

import Ionicons from '@expo/vector-icons/Ionicons';
const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const TabView = () => {
  return(
    <Tab.Navigator 
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === Screens.HOME) {
              iconName = focused
                ? 'ios-information-circle'
                : 'ios-information-circle-outline';
            } else if (route.name === Screens.WEATHER) {
              iconName = focused ? 'ios-list-box' : 'ios-list';
            }

            // You can return any component that you like here!
            return <Ionicons name={iconName} size={size} color={color} />;
          },
        })}
        tabBarOptions={{
          activeTintColor: 'tomato',
          inactiveTintColor: 'gray',
        }}>
        <Tab.Screen  name={Screens.HOME} component={Home} options={{
            headerShown: false
          }} />
        <Tab.Screen name={Screens.WEATHER} component={Weather} />
      </Tab.Navigator>
  )
}
const App = (props) => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen 
          name={Screens.LOGIN} 
          component={Login} 
          options={{
            headerShown: false
          }}
        />
        <Stack.Screen options={{
            headerShown: false
          }} name={Screens.TAB}  component={TabView} />
        <Stack.Screen name={Screens.REG} component={Register} />
        <Stack.Screen name={Screens.VWEATHER} component={ViewWeather} />
        <Stack.Screen name={Screens.WEB} component={ShowView}  options={{headerShown: false}}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;